'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglifyjs');
var watch = require('gulp-watch');
var csso = require('gulp-csso');
var streamqueue  = require('streamqueue');
var svgSprite = require("gulp-svg-sprites");

// Компиляция Sass файлов в файл style.css
gulp.task('sass', function () {
  return gulp.src('./css/**/*.scss')
    .pipe(sass({outputStyle: 'normal'}).on('error', sass.logError))
    .pipe(autoprefixer({
            browsers: ['last 2 versions', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'],
            cascade: false
        }))
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./css/'))
});

//сборка всех файлов в единый bunlde.css
gulp.task('css-build', function() {
  return gulp.src(["./css/**/*.css", "!./css/bundle.css"])
    .pipe(concat('bundle.css'))
    .pipe(gulp.dest('./css/'));
});

//сборка всех файлов из jslib в единый all.js
gulp.task('scripts', function() {
  return streamqueue({ objectMode: true },
        gulp.src('./jslib/jquery-3.2.1.js'),
        gulp.src('./jslib/jquery.fancybox.js'),
        gulp.src('./jslib/jquery.fullPage.js'),
        gulp.src('./jslib/slick.js'),
        gulp.src([
            './jslib/**/*.js',
            '!./jslib/jquery-3.2.1.js',
            '!./jslib/jquery.fullPage.js',
            '!./jslib/slick.js',
            '!./jslib/jquery.fancybox.js'
        ])

    )
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./js/'))
});

//сборка SVG спрайта
gulp.task('svg-sprites', function () {
    return gulp.src('./img/*.svg')
        .pipe(svgSprite({
        	mode: "symbols",
            preview: false
        }))
        .pipe(concat('svg.php'))
        .pipe(gulp.dest("./img/sprites"));
});

//gulp wath - функция которая смотрит за изменениями всех используемых в проекте фейлов и если какой-либо файл был изменен, то перезапускает необходимую gulp задачу для сборки этого файла
gulp.task('watch',['sass', 'scripts', 'css-build'], function() {
gulp.watch('./css/**/*.scss', ['sass']);
 gulp.watch('./**/*.html');
  gulp.watch('./jslib/**/*.js', ['scripts']);
   gulp.watch('./css/**/*.css', ['css-build']);
    gulp.watch('./img/*.svg', ['svg-sprites']);

});

//сжатие всех js файлов для запуска в продакшн, не добавлено в dev, потому-что занимает значительное время
gulp.task('js-prod', function() {
  return streamqueue({ objectMode: true },
        gulp.src('./jslib/jquery-3.2.1.js'),
        gulp.src('./jslib/jquery.fullPage.js'),
        gulp.src('./jslib/fullpage-set.js'),
        gulp.src('./jslib/jquery.fancybox.js')
    )
  	.pipe(uglify())
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./js/'));
});

//сжатие всех css файлов для запуска в продакшн, не добавлено в dev, потому-что занимает значительное время
gulp.task('css-prod', function () {
     return gulp.src(["./css/**/*.css", "!./css/bundle.css"])
        .pipe(csso())
        .pipe(concat('bundle.css'))
        .pipe(gulp.dest('./css/'));
});

gulp.task('dev', ['sass', 'scripts', 'css-build', 'watch', 'svg-sprites']);

gulp.task('build', ['sass', 'js-prod', 'css-prod', 'svg-sprites' ]);

gulp.task('default', ['dev']);
